# About

This is a Node.js starter project.  

- The the package.json is already set up to handle Gulp to lint and minify JS files and to compile SASS files.
- This project uses Express 4 with handlebars templates enabled.
- Static files are served from the public folder divided between src and eist folders.  Processed JS and CSS files are found in teh dist folder.