var express = require('express');
var router = express.Router();

var info = {
  title: 'Starter Project',
  features: ['gulp', 'express 4', 'sass']
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: info.title, features: info.features });
});

module.exports = router;
