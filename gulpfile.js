var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

//js linter
gulp.task('lint', function() {
  return gulp.src('public/src/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

//compile sass
gulp.task('sass', function() {
    return gulp.src('public/src/scss/**/*.scss')
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('public/dist/css'));
});

//minify js
gulp.task('minify', function() {
  return gulp.src('public/src/js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('public/dist/js'))
    .pipe(rename('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/dist/js'));
});

//watcher
gulp.task('watch', function() {
  gulp.watch('public/src/js/**/*.js', ['lint', 'minify']);
  gulp.watch('public/src/scss/**/*.scss', ['sass']);
});

//default task
gulp.task('default', ['lint', 'minify', 'sass']);
